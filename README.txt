Sometimes it is required to execute some actions at the end on page script execution (in a shutdown function).
E.g. to trigger new updating a node when the node is re-saved. This module allows to do it with Rules.

After module installation in the list of the Rules actions there appears "Shutdown Components" section with the list
of available components. Usage is similar to using the native Rules components: one can add any existing component
for postponed execution.