<?php
/**
 * @file
 * Rules Shutdown rules functions.
 */

/**
 * Implements hook_rules_category_info().
 */
function rules_shutdown_rules_category_info() {
  return array(
    'rules_shutdown' => array(
      'label' => t('Shutdown Components'),
      'equals group' => t('Shutdown Components'),
      'weight' => 51,
    ),
  );
}

/**
 * Implements hook_rules_action_info().
 */
function rules_shutdown_rules_action_info() {

  $defaults = array(
    'group' => t('Shutdown Components'),
    'base' => 'rules_shutdown_invoke_component',
    'named parameter' => TRUE,
    'access callback' => 'rules_shutdown_invoke_component_access_callback',
  );

  $items = array();
  foreach (rules_get_components(FALSE, 'action') as $name => $config) {
    $items['shutdown_' . $name] = $defaults + array(
        'label' => t('Shutdown') . ' ' . $config->plugin() . ': ' . drupal_ucfirst($config->label()),
        'parameter' => $config->parameterInfo(),
      );
    $items['shutdown_' . $name]['#config_name'] = $name;
  }
  return $items;
}

/**
 * Access callback for the invoke component condition/action.
 */
function rules_shutdown_invoke_component_access_callback($type, $name) {
  // Cut of the leading 'shutdown_' from the action name.
  $component = rules_config_load(substr($name, 9));

  if (!$component) {
    // Missing component.
    return FALSE;
  }
  // If access is not exposed for this component, default to component access.
  if (empty($component->access_exposed)) {
    return $component->access();
  }
  // Apply the permissions.
  return user_access('bypass rules access') || user_access("use Rules component $component->name");
}

/**
 * Implements RulesPluginUIInterface::operations() for the action.
 */
function rules_shutdown_invoke_component_operations(RulesPlugin $element) {
  $defaults = $element->extender('RulesPluginUI')->operations();
  $info = $element->info();

  // Add an operation for editing the component.
  $defaults['#links']['component'] = array(
    'title' => t('edit component'),
    'href' => RulesPluginUI::path($info['#config_name']),
  );
  return $defaults;
}
